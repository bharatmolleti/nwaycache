// NwayCache.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include <cstdlib>	// for malloc
#include <cstdio>	// for printf
#include <cmath> // for pow

#define LOGE printf
#define LOGI printf

// so , this is the elementary data structure.
// this was meant to , you know,
// fill up this struct with the required info, and pass it to the function
// isThisDecoderStatePossible(VTCache* state);
// this function will actually open up the number of decoders as being instructed by this struct, 
// and will update the nPossible flag.

typedef struct
{
	int nInstances;	// this holds the no of widths and heights held by this structure.
	int* widths;	// the list of widths.
	int* heights;	// the list of heights.
	int nPossible;	// if the device supports this kind of arrangement.
}VTCache;	// cute for video translation cache.

const int VT_INSTANCES_MAX = 4;

const int RES_MAX = 4;

const int RES_WIDTHS[RES_MAX] = { 3840, 1920,  1280, 720};

const int RES_HEIGHTS[RES_MAX] = { 2160 , 1088, 720, 480 };

VTCache*** pTheCache = NULL;

/*
	tree
	|----- (4) (cache line)
	|-------------------(element)- (16)
	|(exponential curse.. 64)
	|(exponential curse.. 256).
	|
*/

// for each of the instance line element, allocate an element.

// once we have the element, we must populate it, with the right mix of params.
// so, each of the instances will have like.. 4 * (instance count) buckets.
// _ _ _ _, 4 buckets for 4 instance case
// each bucket representing a decoder state. 
// its more like a binary counter, so, if a bucket reaches max, increment the next bucket with 1 and flush this.
// it will end when all 4 buckets are full. :) anyway, we know when this will happen.. nElement count.

typedef struct BucketMaster{
	int m_nDecoders;
	int* m_pDecoderResolution;

	BucketMaster ()
	{
		m_nDecoders = 0;
		m_pDecoderResolution = NULL;
	}

	int init(int noOfDecoders)
	{
		m_nDecoders = noOfDecoders;
		m_pDecoderResolution = (int*) malloc(sizeof(int) * m_nDecoders);
		if (m_pDecoderResolution == NULL)
		{
			return -1;
		}
		for (int i = 0; i < m_nDecoders; i++)
		{
			m_pDecoderResolution[i] = 0;
		}
		return 0;
	}

	int close()
	{
		if (m_pDecoderResolution)
		{
			free (m_pDecoderResolution);
			m_pDecoderResolution = NULL;
		}
		return 0;
	}

	int inc(int nIndex = 0)
	{
		m_pDecoderResolution[nIndex]++;
		if (m_pDecoderResolution[nIndex] < RES_MAX)
		{
			return 0;
		}
		m_pDecoderResolution[nIndex] = 0;
		return inc (nIndex+1);
	}

	void printBucketState()
	{
		printf ("\n");
		for( int i = 0; i < m_nDecoders; i++)
		{
			printf(" [ %d ] |", m_pDecoderResolution[i]);
		}
	}
}TBucketMaster;

// the main data structures are done.. and now, to our program.

int _tmain(int argc, _TCHAR* argv[])
{
	
	// a tree that will hold the instance lines.
	pTheCache = (VTCache ***) malloc (sizeof(VTCache**) * VT_INSTANCES_MAX);
	LOGI("\n Cache has been allocated at : %p", pTheCache);

	if(pTheCache == NULL)
	{
		LOGE("\n Cache Allocation Failed at %d ", __LINE__);
		return -1;
	}

	// for each of the instances, we will get to cache the transition states.
	for( int i = 0; i < VT_INSTANCES_MAX; i++)
	{
		LOGI("\n The Required Adress is %p, was: %p", (pTheCache + i), pTheCache[i]);

		int nElementCount = (int) pow ((double)RES_MAX, (i+1)); // look at the (i+1), the +1 is important
		LOGI("\n So , for the instance count of %d, we will have like %d cache elements", i, nElementCount);

		// allocating the cache instance line.
		pTheCache[i] = (VTCache**) malloc (sizeof(VTCache*) * nElementCount);
		if(pTheCache[i] == NULL)
		{
			LOGE("\n memory allocation failed for cacheline : %d ", i);
			return -1;
		}
		LOGI("\n At Adress %p, now is: %p", (pTheCache + i), pTheCache[i]);		

		TBucketMaster bucket;
		bucket.init((i+1));

		int* nDecoderBucket = NULL;
		nDecoderBucket = (int*) malloc(sizeof(int) * i);
		for( int j = 0; j < nElementCount; j++)
		{
			pTheCache[i][j] = (VTCache*) malloc (sizeof(VTCache));
			if (pTheCache[i][j] == NULL)
			{
				LOGE ("\n Memory allocation failed for element : %d in the cache line : %d ", j, i);
				return -1;
			}
			// assign the element as needed
			pTheCache[i][j]->nInstances = (i+1);
			pTheCache[i][j]->widths = (int*) malloc(sizeof(int)*(i+1));
			pTheCache[i][j]->heights = (int*) malloc(sizeof(int)*(i+1));

			for( int k = 0; k < (i+1); k++)
			{
				pTheCache[i][j]->widths[k] = RES_WIDTHS[bucket.m_pDecoderResolution[k]];
				pTheCache[i][j]->heights[k] = RES_HEIGHTS[bucket.m_pDecoderResolution[k]];
			}

			pTheCache[i][j]->nPossible = 0; // ;) determine and assign the state here? next step : profit? 
			bucket.printBucketState ();			
			if (j < (nElementCount -1 )) bucket.inc ();
		}
		bucket.close();
	}

	//  print the ds
	for( int i = 0; i < VT_INSTANCES_MAX; i++)
	{
		printf ("\n Instance Count : %d ", (i+1));
		int nElementCount = (int) pow ((double)RES_MAX, (i+1));
		for( int j = 0; j < nElementCount; j++)
		{
			printf("\n(%d) N = %5d", j, pTheCache[i][j]->nInstances);
			printf("\n W = ");
			for( int k = 0; k < (i+1); k++)
			{
				printf("%d , ", pTheCache[i][j]->widths[k]);
			}
			printf("\n H = ");
			for( int k = 0; k < (i+1); k++)
			{
				printf("%d , ", pTheCache[i][j]->heights[k]);
			}
			printf("\n Possible = %d", pTheCache[i][j]->nPossible);
		}
	}

	// retreive a particular element?
	// nInstances = 3;
	// decoders = 1280, 1280, 1280, // fine take the code  = 2
	// dude.. you have this insane property.. remember the binary based math..
	// you can use it here.
	// for the example below
	// you'll nInstances = 3, this means you have 3 decoders.
	// you need decoders = 1280, 1280, 1280. , this means as per the enum directive. 
	// you'll need elements with code 2 because {3840 (0), 1920 (1), 1280(2), 720 (3)}
	// now you know the elements get generated in this way, so your needed element is @
	// 2	1	0	<- poition base
	// 4    4   4	<- no of resolutions possible
	// ____________
	// 2    2   2	<- needed resolution codes.
	//
	// so based on above, your needed code would be at -> 
	// ( 4* 4)* 2 + (4 * 2) + 2 = 32 + 8 + 2 = 42;
	// based on result.txt :), its correct.
	// now, lets get it for 4 instance, 1280, 720, 3840, 1920
	// 3	2	1	0
	// 4	4	4	4
	//---------------
	// 2	3	0	1
	// (4*4*4)*2 + (4*4)*3 + (4) * 0 + 1 = 128 + 48 + 1 = 177
	// yay.. correct.. :). . party time.. 
	printf ("\n Find nInstances = 3 AND decoders = 1280, 1280, 1280");
	int nInstance = 3;
	int nCode = 2;
	nInstance --;
	printf("\n N = %5d", pTheCache[nInstance][nCode]->nInstances);
	printf("\n W = ");
	for( int k = 0; k < (nInstance+1); k++)
	{
		printf("%d , ", pTheCache[nInstance][nCode]->widths[k]);
	}
	printf("\n H = ");
	for( int k = 0; k < (nInstance+1); k++)
	{
		printf("%d , ", pTheCache[nInstance][nCode]->heights[k]);
	}
	printf("\n Possible = %d", pTheCache[nInstance][nCode]->nPossible);

	// free the ds
	for( int i = 0; i < VT_INSTANCES_MAX; i++)
	{
		int nElementCount = (int) pow ((double)RES_MAX, (i+1));
		for( int j = 0; j < nElementCount; j++)
		{
			if (pTheCache[i][j]->widths != NULL)
			{
				free (pTheCache[i][j]->widths);
				pTheCache[i][j]->widths = NULL;
			}

			if (pTheCache[i][j]->heights != NULL)
			{
				free(pTheCache[i][j]->heights);
				pTheCache[i][j]->heights = NULL;
			}

			if(pTheCache[i][j] != NULL) 
			{
				free(pTheCache[i][j]);
				pTheCache[i][j] = NULL;
			}
		}

		if(pTheCache[i] != NULL)
		{
			free (pTheCache[i]);
			pTheCache[i]  = NULL;
		}
	}
	
	free (pTheCache);
	pTheCache = NULL;
	char ch = 'a';
	scanf("%c", &ch);
	return 0;
}